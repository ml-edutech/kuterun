# Kuterun -- kubernetes tests runner

Uses kubernetes API to run pods, which test user code.

TODO:
 - [ ] versioning
 - [ ] prober
 - [ ] try to reconnect when probes are failing
 - [ ] liveness readiness for k8s
 - [ ] profiling


## Build
```
make docker
```

## Deploy
```
make deploy
```

## Lint example
```
golangci-lint run internal/operator --enable-all --disable wsl --disable nlreturn
```

